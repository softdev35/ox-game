
import java.util.Scanner;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Sarocha
 */
public class Game {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        boolean check = false;
        int row, col;
        int count = 9;
        char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};

        System.out.println("Welcome to OX Game");

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(table[i][j] + " ");
            }
            System.out.println();
        }
        while (check == false) {
            if (table[0][0] != '-' && table[0][1] != '-' && table[0][2] != '-'
                    && table[1][0] != '-' && table[1][1] != '-' && table[1][2] != '-'
                    && table[2][0] != '-' && table[2][1] != '-' && table[2][2] != '-') {
                System.out.println(">>>Draw<<<");
                break;
            }

            System.out.println();
            System.out.println("Turn O");
            System.out.println("Please input row, col:");

            row = kb.nextInt();
            col = kb.nextInt();
            while (row > 2 || row < 0 || col > 2 || col < 0) {
                System.out.println("Please input number 0-2");
                System.out.println("Turn O");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            while (table[row][col] != '-') {
                System.out.println("This posision has already been used. Pless try again.");
                System.out.println("Turn O");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            count--;
            table[row][col] = 'O';
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j] + " ");
                }
                System.out.println();
            }

            if (table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-'
                    || table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-'
                    || table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-'
                    || table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-'
                    || table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-'
                    || table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-'
                    || table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-'
                    || table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[0][2] != '-') {
                check = true;
                System.out.println();
                System.out.println(">>>O Win<<<");
                break;
            }

            if (count == 0) {
                System.out.println();
                System.out.println(">>>Draw<<<");
                break;
            }

            System.out.println();
            System.out.println("Turn X");
            System.out.println("Please input row, col:");

            row = kb.nextInt();
            col = kb.nextInt();
            while (row > 2 || row < 0 || col > 2 || col < 0) {
                System.out.println("Please input number 0-2");
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            while (table[row][col] != '-') {
                System.out.println("This posision has already been used. Pless try again.");
                System.out.println("Turn X");
                System.out.println("Please input row, col:");
                row = kb.nextInt();
                col = kb.nextInt();
            }
            count--;
            table[row][col] = 'X';
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    System.out.print(table[i][j] + "  ");
                }
                System.out.println();
            }

            if (table[0][0] == table[0][1] && table[0][1] == table[0][2] && table[0][0] != '-'
                    || table[1][0] == table[1][1] && table[1][1] == table[1][2] && table[1][0] != '-'
                    || table[2][0] == table[2][1] && table[2][1] == table[2][2] && table[2][0] != '-'
                    || table[0][0] == table[1][0] && table[1][0] == table[2][0] && table[0][0] != '-'
                    || table[0][1] == table[1][1] && table[1][1] == table[2][1] && table[0][1] != '-'
                    || table[0][2] == table[1][2] && table[1][2] == table[2][2] && table[0][2] != '-'
                    || table[0][0] == table[1][1] && table[1][1] == table[2][2] && table[0][0] != '-'
                    || table[0][2] == table[1][1] && table[1][1] == table[2][0] && table[0][2] != '-') {
                check = true;
                System.out.println();
                System.out.println(">>>X Win<<<");
                break;
            }
            if (count == 0) {
                System.out.println();
                System.out.println(">>>Draw<<<");
                break;
            }

        }
    }

}
